<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Woocommerce_Advanced_Shipping_Extension
 * @subpackage Woocommerce_Advanced_Shipping_Extension/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Woocommerce_Advanced_Shipping_Extension
 * @subpackage Woocommerce_Advanced_Shipping_Extension/public
 * @author     # <#>
 */
class Woocommerce_Advanced_Shipping_Extension_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Advanced_Shipping_Extension_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Advanced_Shipping_Extension_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/woocommerce-advanced-shipping-extension-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Advanced_Shipping_Extension_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Advanced_Shipping_Extension_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/woocommerce-advanced-shipping-extension-public.js', array( 'jquery' ), $this->version, false );

	}
	
	/**
	 * Returns the number of completed orders by customer_id
	 *
	 * @since    1.0.0
	 */
	 
	public function debug_function() { 
		/*
		$current_user_id = get_current_user_id();
		$rating = $this->get_customer_completed_orders_count($current_user_id);
		echo 'this: ' . $rating;
		*/
	}
	
	/**
	 * Returns the number of completed orders by customer_id
	 *
	 * @since    1.0.0
	 */
	public function get_customer_completed_orders_count($customer_id) {
		$args = array(
			'customer_id' =>$customer_id,
			'post_status' => 'completed',
			'post_type' => 'shop_order',
			'return' => 'ids',
		);
		
		return count( (array) wc_get_orders( $args ) ); // count the array of orders

	}

	/**
	 * Adds the completed_orders condition
	 *
	 * @since    1.0.0
	 */
	
	public function add_order_sent_count_condition($conditions) {
		$conditions['User Details'] += array('completed_orders' => 'Completed Orders');
		return $conditions;
	}

    /**
     * Adds the subtotal_[shipping_class] conditions
     *
     * @since    1.0.0
     */

    public function add_cart_subtotal_bc1_condition($conditions) {
        $conditions['Cart'] += array('subtotal_bc1' => 'Subtotal (bc1)');
        return $conditions;
    }

    public function add_cart_subtotal_on1_condition($conditions) {
        $conditions['Cart'] += array('subtotal_on1' => 'Subtotal (on1)');
        return $conditions;
    }
	
		
	/**
	 * Adds the completed_orders condition
	 *
	 * @since    1.0.0
	 */
	 
	public function was_match_condition_values_completed_orders($condition) {
		$condition[] = 'completed_orders';

		return $condition;
	}

    /**
     * Adds the subtotal_[shipping_class] conditions
     *
     * @since    1.0.0
     */

    public function was_match_condition_values_subtotal_bc1($condition) {
        $condition[] = 'subtotal_bc1';
        return $condition;
    }

    public function was_match_condition_values_subtotal_on1($condition) {
        $condition[] = 'subtotal_on1';
        return $condition;
    }



	/**
	 * Adds the completed_orders hook
	 *
	 * @since    1.0.0
	 */
	
	public function was_match_condition_completed_orders_check($match, $operator, $value, $package) {
		
		if ( is_user_logged_in() ) {
			
			$current_user_id = get_current_user_id();
			
			// see if rating exits
			if ( class_exists( 'BM' ) ) {
				$rating = BM()->budmail_customer_rating($current_user_id);	
			} 
			// else use get_is_paying_customer or count method
			else {
				//$customer = new WC_Customer( $current_user_id );
				//$rating = $customer->get_is_paying_customer() ? 1 : 0;
				$rating = $this->get_customer_completed_orders_count($current_user_id);
			}
			
			if ( '==' == $operator ) :
				$match = ( $rating == $value );
			elseif ( '!=' == $operator ) :
				$match = ( $rating != $value );
			elseif ( '>=' == $operator ) :
				$match = ( $rating >= $value );
			elseif ( '<=' == $operator ) :
				$match = ( $rating <= $value );
			endif;
		
		}
		
		return $match;
	}

    /**
     * Shipping class bc1
     *
     * Matches if the condition value shipping class is in the cart.
     *
     * @since 1.0.1
     *
     * @param  bool   $match    Current match value.
     * @param  string $operator Operator selected by the user in the condition row.
     * @param  mixed  $value    Value given by the user in the condition row.
     * @param  array  $package  List of shipping package details.
     * @return BOOL             Matching result, TRUE if results match, otherwise FALSE.
     */
    public function was_match_condition_subtotal_bc1_check( $match, $operator, $value, $package ) {

        $price = 0;
        foreach ( $package['contents'] as $key => $item ) :
            $id      = ! empty( $item['variation_id'] ) ? $item['variation_id'] : $item['product_id'];
            $product_data = wc_get_product( $id );
            if ( $product_data->get_shipping_class() == 'bc1' ) {
                // $product = $item['data'];
                $price += $item['line_subtotal'];
            }

        endforeach;

        $value = (string) $value;

        // Make sure its formatted correct
        $value = str_replace( ',', '.', $value );

        if ( '==' == $operator ) :
            $match = ( $price == $value );
        elseif ( '!=' == $operator ) :
            $match = ( $price != $value );
        elseif ( '>=' == $operator ) :
            $match = ( $price >= $value );
        elseif ( '<=' == $operator ) :
            $match = ( $price <= $value );
        endif;

        return $match;

    }

    /**
     * Shipping class - on1
     *
     * Matches if the condition value shipping class is in the cart.
     *
     * @since 1.0.1
     *
     * @param  bool   $match    Current match value.
     * @param  string $operator Operator selected by the user in the condition row.
     * @param  mixed  $value    Value given by the user in the condition row.
     * @param  array  $package  List of shipping package details.
     * @return BOOL             Matching result, TRUE if results match, otherwise FALSE.
     */
    public function was_match_condition_subtotal_on1_check( $match, $operator, $value, $package ) {

        $price = 0;
        foreach ( $package['contents'] as $key => $item ) :
            $id      = ! empty( $item['variation_id'] ) ? $item['variation_id'] : $item['product_id'];
            $product_data = wc_get_product( $id );
            if ( $product_data->get_shipping_class() == 'on1' ) {
                // $product = $item['data'];
                $price += $item['line_subtotal'];
            }
        endforeach;

        $value = (string) $value;

        // Make sure its formatted correct
        $value = str_replace( ',', '.', $value );

        if ( '==' == $operator ) :
            $match = ( $price == $value );
        elseif ( '!=' == $operator ) :
            $match = ( $price != $value );
        elseif ( '>=' == $operator ) :
            $match = ( $price >= $value );
        elseif ( '<=' == $operator ) :
            $match = ( $price <= $value );
        endif;

        return $match;

    }

}
