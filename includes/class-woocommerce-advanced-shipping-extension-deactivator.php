<?php

/**
 * Fired during plugin deactivation
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Woocommerce_Advanced_Shipping_Extension
 * @subpackage Woocommerce_Advanced_Shipping_Extension/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Woocommerce_Advanced_Shipping_Extension
 * @subpackage Woocommerce_Advanced_Shipping_Extension/includes
 * @author     # <#>
 */
class Woocommerce_Advanced_Shipping_Extension_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
