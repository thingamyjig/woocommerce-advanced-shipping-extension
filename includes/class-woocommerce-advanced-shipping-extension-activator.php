<?php

/**
 * Fired during plugin activation
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Woocommerce_Advanced_Shipping_Extension
 * @subpackage Woocommerce_Advanced_Shipping_Extension/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woocommerce_Advanced_Shipping_Extension
 * @subpackage Woocommerce_Advanced_Shipping_Extension/includes
 * @author     # <#>
 */
class Woocommerce_Advanced_Shipping_Extension_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		
		// Require parent plugin
		if ( ! is_plugin_active( 'woocommerce-advanced-shipping/woocommerce-advanced-shipping.php' ) and current_user_can( 'activate_plugins' ) ) {
			// Stop activation redirect and show error
			wp_die('Sorry, but this plugin requires the WooCommerce Advanced Shipping plugin to be installed and active. <br><a href="' . admin_url( 'plugins.php' ) . '">&laquo; Return to Plugins</a>');
		}

	}

}
