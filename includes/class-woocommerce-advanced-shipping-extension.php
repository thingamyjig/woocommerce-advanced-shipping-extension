<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Woocommerce_Advanced_Shipping_Extension
 * @subpackage Woocommerce_Advanced_Shipping_Extension/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Woocommerce_Advanced_Shipping_Extension
 * @subpackage Woocommerce_Advanced_Shipping_Extension/includes
 * @author     # <#>
 */
class Woocommerce_Advanced_Shipping_Extension {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Woocommerce_Advanced_Shipping_Extension_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'WOOCOMMERCE_ADVANCED_SHIPPING_EXTENSION_VERSION' ) ) {
			$this->version = WOOCOMMERCE_ADVANCED_SHIPPING_EXTENSION_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'woocommerce-advanced-shipping-extension ';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Woocommerce_Advanced_Shipping_Extension_Loader. Orchestrates the hooks of the plugin.
	 * - Woocommerce_Advanced_Shipping_Extension_i18n. Defines internationalization functionality.
	 * - Woocommerce_Advanced_Shipping_Extension_Admin. Defines all hooks for the admin area.
	 * - Woocommerce_Advanced_Shipping_Extension_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-advanced-shipping-extension-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-advanced-shipping-extension-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-woocommerce-advanced-shipping-extension-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-woocommerce-advanced-shipping-extension-public.php';

		$this->loader = new Woocommerce_Advanced_Shipping_Extension_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Woocommerce_Advanced_Shipping_Extension_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Woocommerce_Advanced_Shipping_Extension_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Woocommerce_Advanced_Shipping_Extension_Admin( $this->get_plugin_name(), $this->get_version() );

		//$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		//$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Woocommerce_Advanced_Shipping_Extension_Public( $this->get_plugin_name(), $this->get_version() );
		
		// sent condition
		$this->loader->add_filter( 'was_conditions', $plugin_public, 'add_order_sent_count_condition', 10, 1 );
        $this->loader->add_filter( 'was_match_condition_values', $plugin_public, 'was_match_condition_values_completed_orders', 10, 1 );
		$this->loader->add_filter( 'was_match_condition_completed_orders', $plugin_public, 'was_match_condition_completed_orders_check', 10, 4 );

		// total by shipping class bc1
        $this->loader->add_filter( 'was_conditions', $plugin_public, 'add_cart_subtotal_bc1_condition', 10, 1 );
        $this->loader->add_filter( 'was_match_condition_values', $plugin_public, 'was_match_condition_values_subtotal_bc1', 10, 1 );
        $this->loader->add_filter( 'was_match_condition_subtotal_bc1', $plugin_public, 'was_match_condition_subtotal_bc1_check', 10, 4 );

        // total by shipping class on1
        $this->loader->add_filter( 'was_conditions', $plugin_public, 'add_cart_subtotal_on1_condition', 10, 1 );
        $this->loader->add_filter( 'was_match_condition_values', $plugin_public, 'was_match_condition_values_subtotal_on1', 10, 1 );
        $this->loader->add_filter( 'was_match_condition_subtotal_on1', $plugin_public, 'was_match_condition_subtotal_on1_check', 10, 4 );

    }

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Woocommerce_Advanced_Shipping_Extension_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
